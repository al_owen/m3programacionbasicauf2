/**
 * 
 */
package hash;

import java.util.HashMap;

/**@author Álvaro Owen de la Quintana
 * ProgramacionBasicaUF2 
 * 13 dic 2022
 */
public class HashMaps {

	public static void main(String[] args) {
		//crear hashmap
		//el hasmap tiene una llave y un valor asociado a ella
		//la llave puedes ser de cualquier tipo (no primitivo) y diferente al valor asociado
		HashMap<String, Pokemon> pokedex = new HashMap<>();
		
		//añadir objetos
		Pokemon pikachu = new Pokemon("Pikachu", 2.5, 1.0);//creamos un objeto de tipo Pokemon
		Pokemon charmander = new Pokemon("Charmander", 2.5, 1.5);

		//para añadir un conjunto <llave, valor> utilizamos put
		pokedex.put("025", pikachu); //añadimos el Pokemon en la llave '025' con el metodo put
		//accedemos al objeto asociado a la key '025' mediante el metodo get
		System.out.println("El pokemon con la key '025' es: "+pokedex.get("025"));
		
		
		//si la llave no existe devuelve null
		pokedex.getOrDefault("", null);
		/*las llaves son unicas, y por lo tanto no se pueden repetir
		 * en caso de poner otro objeto en una llave existente no se duplicara,
		 * sino, se reemplazará*/
		pokedex.put("025", charmander);
		System.out.println("El nuevo pokemon con la key '025' es: "+pokedex.get("025"));
		
		pokedex.put("025", pikachu); //añadimos el Pokemon en la llave '025' con el metodo put
		pokedex.put("004", charmander);
		/*Si queremos modificar el objeto existente, podemos guardar la referencia
		 * dentro de una variable y modificarlo desde allí, al ser una referencia, 
		 * modificara el objeto en cuestion*/
		
		Pokemon pokachu = pokedex.get("025");
		pokachu.especie = "Pokachu";
		pokachu.mida = 8.8;
		Pokemon mimikyu = new Pokemon("Mimikyu", 4, 3);
		pokedex.put("025", mimikyu);
		System.out.println("El pokemon con la key '025' ahora es: "+pokedex.get("025"));
		
		/*Para recorrer el hashmap y ver los datos, lo podemos hacer con un foreach
		 * que se puede recorrer la lista de valores o de 'keys'*/
		//recorremos los valores
		for (Pokemon poke: pokedex.values()) {
			System.out.println(poke);
		}
		//recorremos las llaves, la ventaja es que mediante la key podemos ver el valor tambien.
		for (String keys : pokedex.keySet()) {
			System.out.println(keys+" ==> "+pokedex.get(keys));
		}
		
		//verificar si una llave existe dentro del hashmap
		System.out.println("'025' existe? " +pokedex.containsKey("025"));
		System.out.println("'028' existe? " +pokedex.containsKey("028"));
		
		//verificar si un objeto existe dentro del hashmap
		Pokemon ditto = new Pokemon("Ditto", 3.5, 0.5); //creamos un objeto pero no lo añadimos al hashmap
		System.out.println("Existe el pokemon? "+pokedex.containsValue(pokachu));
		System.out.println("Existe el pokemon?"+pokedex.containsValue(ditto));
		
		/*Si queremos evitar que se sobreescriban los valores asociados a una key
		 * utilizamos el metodo putIfAbsent, que nos añadira la llave y el valor solo si la llave no existe*/
		pokedex.putIfAbsent("004", ditto);
		//la llave existe, por lo tanto no se modifica el valor de la llave '004'
		System.out.println("El pokemon con la key '004' ahora es: "+pokedex.get("004"));

		
		/*Podemos eliminar elementos <Llave, valor> utilizando el metodo remove, y pasandole la clave*/
		pokedex.putIfAbsent("009", ditto);
		System.out.println("El pokemon con la key '009' es: "+pokedex.get("009"));
		
		//eleminamos el elemento asociado a la llave '009' solo si ambos son correctos
		pokedex.remove("009", charmander);//charmander no esta asociado a la key '009' 
		//por lo tanto no se elimina
		System.out.println("El pokemon con la key '009' es: "+pokedex.get("009"));
		
		pokedex.remove("009", ditto);//dito si esta asociado a la key '009'
		//por lo tanto si se elimina
		System.out.println("El pokemon con la key '009' es: "+pokedex.get("009"));
		
	}
}
