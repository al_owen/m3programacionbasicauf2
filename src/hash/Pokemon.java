/**
 * 
 */
package hash;

/**@author Álvaro Owen de la Quintana
 * ProgramacionBasicaUF2 
 * 13 dic 2022
 */
public class Pokemon {
	
	//Atributos 
	//suelen ser private y no statics
	String especie;
	double peso;
	double mida;
	
	//constructor
	public Pokemon(String especie, double peso, double mida) {
		this.especie = especie;
		this.peso = peso;
		this.mida = mida;
		cosas(5);
	}
	
	/**
	 * Funcion que hace cosas
	 * @param numero
	 */
	private void cosas(int numero){
		//TODO
	}
	
	@Override
	public String toString() {
		return "Pokemon [especie=" + especie + ", peso=" + peso + ", mida=" + mida + "]";
	}
	
	
	
}
