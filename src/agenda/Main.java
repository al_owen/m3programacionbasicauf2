/**
 * 
 */
package agenda;

import java.time.LocalDate;

import funciones.UtilConsole;

/**@author Álvaro Owen de la Quintana
 * ProgramacionBasicaUF2 
 * 1 dic 2022
 */
public class Main {

	public static void main(String[] args) {
		Persona p = new Persona(8, "26544r", "Alvaro", null, UtilConsole.demanarData(), null, null, null);
		System.out.println(p);
		//Persona p2 = new Persona();
		//p2.setFechaDeNacimiento(LocalDate.now());
		
		//System.out.println(Persona.diferenciaEdad(p, p2));
		
		
		Adreca a = new Adreca("Badia", "Barcelona","08205", "Carrer Ibiza, 5");
		System.out.println(a);
		
	}
	
}
