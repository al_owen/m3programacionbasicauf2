/**
 * 
 */
package agenda;

import java.util.ArrayList;
import java.util.HashMap;

import funciones.UtilConsole;

/**@author Álvaro Owen de la Quintana
 * ProgramacionBasicaUF2 
 * 12 dic 2022
 */
public class Controlador {

	private static HashMap<Integer, Persona> persones = new HashMap<>();
	
	public static void main(String[] args) {
		int opcion = -1;
		do {
			menu();
			opcion = UtilConsole.pedirInt();
			switch (opcion) {
			case 1:
				afegirPersona();
				break;
			case 4:
				modificarPersona();
				break;
			default:
				throw new IllegalArgumentException("Unexpected value: " + opcion);
			}
			
		} while (opcion != 0);
	}

	/**
	 * 
	 */
	private static void modificarPersona() {
		//pedimos el id para comprobar si existe o no 
		int id = UtilConsole.pedirInt();
		if (persones.containsKey(id)) {
			//guardamos en una variable el valor asociado a la id
			Persona p = persones.get(id);
			//pedimos los valores para modificar en la persona
			p.setIdPersona(id);
			p.setNombre(UtilConsole.pedirString());
			p.setApellido(UtilConsole.pedirString());
			p.setDni(UtilConsole.pedirDNI());
			//creamos una direccion
			String poblacion = UtilConsole.pedirString();
			String provincia = UtilConsole.pedirString();
			String cp = UtilConsole.pedirCP();
			String domicilio = UtilConsole.pedirString();
			Adreca ad = new Adreca(poblacion, provincia, cp, domicilio);
			
			//le modificamos la direccion
			p.setDireccion(ad);
			//volvemos a añadir a persona, con la misma llave para que la sobreescriba
			persones.put(id, p);
		}
		
		
	}

	/**
	 * @param persones 
	 * 
	 */
	private static void afegirPersona() {
		//pedimos un id para verificar si ya existe la llave
		int id = UtilConsole.pedirInt();
		//si no existe creamos la persona
		if (!persones.containsKey(id)) {
			//creamos a la persona sin nada
			Persona p = new Persona();
			//modificamos sus datos
			p.setIdPersona(id);
			p.setNombre(UtilConsole.pedirString());
			p.setApellido(UtilConsole.pedirString());
			p.setDni(UtilConsole.pedirDNI());
			p.setFechaDeNacimiento(UtilConsole.demanarData());
			persones.put(id, p);//la agregamos al hashmap 
		}else {
			System.out.println("La persona ya existe");
		}
		
		
	}

	/**
	 * 
	 */
	private static void menu() {
		System.out.println("0. Sortir");
		System.out.println("1. Afegir persona");
		System.out.println("2. Buscar persona");
		System.out.println("3. Modificar persona");
		System.out.println("4. Eliminar persona");
		System.out.println("5. Diferencia edad");
		System.out.println("6. Mostrar persones");
		
	}
	
}
