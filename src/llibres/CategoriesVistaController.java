/**
 * 
 */
package llibres;

import java.util.HashMap;

import funciones.UtilConsole;

/**
 * @author Álvaro Owen de la Quintana ProgramacionBasicaUF2 10 ene 2023
 */
public class CategoriesVistaController {

	public static void inici(HashMap<Integer, Categoria> categories) {
		int opcion = 0;
		do {
			System.out.println("0. Salir");
			System.out.println("1. Crear categoria");
			System.out.println("2. Borrar categoria");
			System.out.println("3. Mostrar categoria");
			System.out.println("4. Ver libros de una categoria");
			opcion = UtilConsole.pedirInt();
			switch(opcion) {
			case 0:
				return;
			case 1:
				crearCategoria(categories);
				break;
			case 2:
				//borrarCategoria();
				break;
			case 3:
				mostrarCategories(categories);
				break;
			case 4:
				//verLibrosCategoria();
				break;
			}

		} while (opcion != 0);
	}

	/**Recorre la lista de categorias y lo muestra uno a uno
	 * @param categories
	 */
	public static void mostrarCategories(HashMap<Integer, Categoria> categories) {
		for (Integer id : categories.keySet()) {
			System.out.println(id +" - "+categories.get(id).getNom());
		}
	}

	/**crea una categoria
	 * @param categories
	 */
	public static Categoria crearCategoria(HashMap<Integer, Categoria> categories) {
		//pide el valor de los atributos necesarios
		System.out.println("Escribe el id");
		int id = UtilConsole.pedirInt();
		System.out.println("Escribe el nombre");
		String name = UtilConsole.pedirString();
		//crea la categoria 
		Categoria cat = new Categoria(id, name);
		//la guarda en la lista de categorias
		categories.put(id, cat);
		//devuelve la categoria en caso de que se necesite
		return cat;
	}
	
	/**
	 * busca una categoria
	 * @param categories
	 * @return
	 */
	public static Categoria getCategoria(HashMap<Integer, Categoria> categories) {
		mostrarCategories(categories);
		System.out.println("Elije el id de la categoria que quieras");
		int num = -1;
		//pide un id y devuelve la categoria asociada 
		//en caso de que el id no exista volvera a preguntar
		while (!categories.containsKey(num)) {
			num = UtilConsole.pedirInt();
		}
		return categories.get(num);
	}

}
