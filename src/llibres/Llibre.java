/**
 * 
 */
package llibres;

import java.io.Serializable;

/**@author Álvaro Owen de la Quintana
 * ProgramacionBasicaUF2 
 * 20 dic 2022
 */
public class Llibre{
	private String titol;
	private String descripcio;
	private double preuVenda;
	private Editor editor;
	private Categoria categoria;
	
	public Llibre(String titol, String descripcio, double preuVenda, Editor editor, Categoria categoria) {
		this.titol = titol;
		this.descripcio = descripcio;
		this.preuVenda = preuVenda;
		this.editor = editor;
		this.categoria = categoria;
	}
	public String getTitol() {
		return titol;
	}
	public void setTitol(String titol) {
		this.titol = titol;
	}
	public String getDescripcio() {
		return descripcio;
	}
	public void setDescripcio(String descripcio) {
		this.descripcio = descripcio;
	}
	public double getPreuVenda() {
		return preuVenda;
	}
	public void setPreuVenda(double preuVenda) {
		this.preuVenda = preuVenda;
	}
	public Editor getEditor() {
		return editor;
	}
	public void setEditor(Editor editor) {
		this.editor = editor;
	}
	public Categoria getCategoria() {
		return categoria;
	}
	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}
	@Override
	public String toString() {
		return titol +", descripcion: " + descripcio + ", precio: " + preuVenda + ", editor:[ "
				+ editor + " ], categoria: [" + categoria+" ]";
	}
	
	
}
