/**
 * 
 */
package llibres;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;

/**@author Álvaro Owen de la Quintana
 * ProgramacionBasicaUF2 
 * 20 dic 2022
 */
public class Fitxers {

	
	public static HashMap<?,?> obrirFitxer(String nomFitxer){

		File fitxer = new File(nomFitxer);
		try {
			fitxer.createNewFile();
		} catch (IOException e) {
			System.out.println("Error creando fichero:" + e);
		}

		try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(fitxer))){
			return (HashMap<?, ?>) ois.readObject();
		} catch (EOFException e){
			//La primera vegada estarà buit però no és un error
		} catch (IOException e){
			//e.printStackTrace();
		} catch (ClassNotFoundException e) {
			System.out.println("La clase no existe:" + e);
		}
		
		return null;
	}

	public static void guardarFitxer(String nomFitxer, HashMap<?,?> mapa){
		try (ObjectOutputStream oo = new ObjectOutputStream(new FileOutputStream(nomFitxer))) {
			oo.writeObject(mapa);
		} catch (IOException e) {
			System.out.println("Error escribiendo fichero:" + e);
		}
	}
}
