package llibres;

public class Editor{
	private int ideditor;
	private String nom;
	private String dni;
	
	public Editor(int ideditor, String nom, String dni) {
		super();
		this.ideditor = ideditor;
		this.nom = nom;
		this.dni = dni;
	}
	
	public int getIdeditor() {
		return ideditor;
	}
	public void setIdeditor(int ideditor) {
		this.ideditor = ideditor;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getDni() {
		return dni;
	}
	public void setDni(String dni) {
		this.dni = dni;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Editor) {
			if (((Editor)obj).getDni().equals(this.dni)) {
				return true;
			}
			return false;
		}else if (obj instanceof String) {
			if (this.dni.equals((String)obj)) {
				return true;
			}
			return false;
		}
		return super.equals(obj);
	}

	@Override
	public String toString() {
		return "Editor " + ideditor + " - nom=" + nom + " - dni=" + dni;
	}
	
	
	
}
