package llibres;

import java.util.HashMap;

import funciones.UtilConsole;

/**
 * @author Álvaro Owen de la Quintana ProgramacionBasicaUF2 9 ene 2023
 */
public class EditorsVistaController {

	public static void inici(HashMap<Integer, Editor> editors) {
		int opcion = 0;
		do {
			System.out.println("0. Salir");
			System.out.println("1. Crear editor");
			System.out.println("2. Borrar editor");
			System.out.println("3. Mostrar editores");
			System.out.println("4. Ver libros de un editor");
			opcion = UtilConsole.pedirInt();
			switch(opcion) {
			case 0:
				return;
			case 1:
				crearEditor(editors);
				break;
			case 2:
				//borrarEditor();
				break;
			case 3:
				mostrarEditores(editors);
				break;
			case 4:
				//verLibrosEditor();
				break;
			}

		} while (opcion != 0);
	}

	/**Recorre la lista de editores y los muestra uno a uno
	 * @param editors
	 */
	public static void mostrarEditores(HashMap<Integer, Editor> editors) {
		for (Editor editor : editors.values()) {
			System.out.println(editor);
		}
	}

	/**Crea un editor y lo guarda en la lista
	 * @param editors
	 */
	public static Editor crearEditor(HashMap<Integer, Editor> editors) {
		//Pide los valores de los parametros 
		System.out.println("Escribe el id");
		int id = UtilConsole.pedirInt();
		System.out.println("Escribe el nombre");
		String name = UtilConsole.pedirString();
		System.out.println("Escribe el DNI");
		String dni = UtilConsole.pedirDNI();
		//crea el editor con los valores pedidos
		Editor editor = new Editor(id, name, dni);
		//compara los editores, y si no existe lo guarda dentro de la lista 
		if (!compararEditors(editor, editors)) {
			editors.put(id, editor);			
		}
		return editor;
	}
	
	public static boolean compararEditors(Editor editor, HashMap<Integer, Editor> editors) {
		//compara los editores y si encuentra alguno igual, devuelve true 
		for (Editor editor2 : editors.values()) {
			if (editor.equals(editor2)) {
				return true;
			}
		}
		return false;
	}
	
	
}
