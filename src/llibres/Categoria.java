package llibres;

public class Categoria {
	private static int numeroCategoria;
	private int idcategoria;
	private String nom;
	

	public Categoria(int id, String name) {
		idcategoria = id;
		nom = name;
		numeroCategoria++;
	}
	
	public int getIdcategoria() {
		return idcategoria;
	}
	public void setIdcategoria(int idcategoria) {
		this.idcategoria = idcategoria;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}

	@Override
	public String toString() {
		return "Categoria: " + nom + " id: "+idcategoria;
	}
	
	
}
