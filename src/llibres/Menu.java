/**
 * 
 */
package llibres;

import java.util.HashMap;

import funciones.UtilConsole;

/**@author Álvaro Owen de la Quintana
 * ProgramacionBasicaUF2 
 * 15 dic 2022
 */
public class Menu {

	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		HashMap<Integer, Llibre> llibres = (HashMap<Integer, Llibre>) Fitxers.obrirFitxer("llibres.alvaro");
		if (llibres == null) {
			llibres = new HashMap<>();
		}
		
		for (Llibre llibre : llibres.values()) {
			System.out.println(llibre);
		}			
		
		//Llibre ll1 = new Llibre(UtilConsole.pedirInt(), UtilConsole.pedirString());
		//llibres.put(ll1.getId(), ll1);
		
		
		Fitxers.guardarFitxer("llibres.alvaro", llibres);
		
	}
}
