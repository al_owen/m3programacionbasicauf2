package llibres;

import java.util.HashMap;

import funciones.UtilConsole;

/**@author Álvaro Owen de la Quintana
 * ProgramacionBasicaUF2 
 * 9 ene 2023
 */
public class IniciVistaControlador {

	//es posible crear los hashmaps en esta clase y pasarlo al resto de clases
	//mediante las funciones. En este caso depende de vosotros.
	//HashMap<String, Llibre> llibres = new HashMap<>();
	
	private static HashMap<Integer, Editor> editors = new HashMap<>();
	private static HashMap<Integer, Categoria> categories = new HashMap<>();
	
	public static void main(String[] args) {
		int opcion = 0;
		
		do {
			//menu
			System.out.println("0. Salir");
			System.out.println("1. Llibres");
			System.out.println("2. Categories");
			System.out.println("3. Editors");
			opcion = UtilConsole.pedirInt();
			//segun lo que se escoge, se llama al submenu del controlador que toca
			if (opcion == 1) {
				LlibresVistaController.inici(editors, categories);
			}else  if (opcion ==2){
				CategoriesVistaController.inici(categories);
			}else  if (opcion == 3){
				EditorsVistaController.inici(editors);
			}

		} while (opcion != 0);	
	}
}
