package llibres;

import java.util.HashMap;

import funciones.UtilConsole;

/**@author Álvaro Owen de la Quintana
 * ProgramacionBasicaUF2 
 * 9 ene 2023
 */
public class LlibresVistaController {

	//crear hasmap de <String, Libro> donde se guardan los libros
	private static HashMap<String, Llibre> llibres = new HashMap<>();
	
	//submenu del controlador
	public static void inici(HashMap<Integer, Editor> editors, HashMap<Integer, Categoria> categories) {
		int opcion = 0;
		do {
			//menu
			System.out.println("0. Salir");
			System.out.println("1. Crear libro");
			System.out.println("2. Borrar libro");
			System.out.println("3. Mostrar libros");
			System.out.println("4. Ver DNI del editor de un libro");
			opcion = UtilConsole.pedirInt();
			switch (opcion) {
			case 0:
				return;
			case 1:
				crearLibro(editors, categories);
				break;
			case 2:
				//borrarLibro();
				break;
			case 3: 
				mostrarLibros();
				break;
			case 4:
				verDniEditor();
				break;
			default:
				break;
			}

		} while (opcion != 0);
	}
	
	/**
	 * Ver el dni del editor
	 */
	private static void verDniEditor() {
		String titulo = UtilConsole.pedirString();
		if (llibres.containsKey(titulo)) {
			String dni = llibres.get(titulo).getEditor().getDni();
			System.out.println("El Dni del editor de el libro: "+titulo+" es "+dni);
		}else {
			System.out.println("El libro no existe!");
		}
	}

	/**
	 * Recorre la lista de libros y se imprime uno a uno
	 */
	private static void mostrarLibros() {
		for(Llibre llibre : llibres.values()) {
			System.out.println(llibre);
		}
	}

	/**Crea un libro
	 * @param categories 
	 * @param editors 
	 * 
	 */
	private static void crearLibro(HashMap<Integer, Editor> editors, HashMap<Integer, Categoria> categories) {
		//Pedimos los valores mediante un scanner
		System.out.println("Escribe el titulo");
		String titol = UtilConsole.pedirString();
		System.out.println("Escribe la descripcion");
		String descripcio = UtilConsole.pedirString();
		System.out.println("Escribe el precio");
		double preuVenda = UtilConsole.pedirDouble();
		//creamos un editor
		Editor editor = null;
		//mientras el editor sea null pedimos una opcion
		while (editor == null) {
			System.out.println("1. Crear un nuevo editor");
			System.out.println("2. Usar un editor ya existente");
			int opcion = UtilConsole.pedirInt();
			if (opcion == 1) {
				//si el usuario quiere crear un nuevo editor, llamamos a la funcion
				//crear editor de la clase EditorsVistaControlador, que una vez creado nos devuelve el editor
				editor = EditorsVistaController.crearEditor(editors);
			}else if (opcion == 2) {
				//si queremos elegir un editor existente mostramos cada editor con su id
				System.out.println("Elige el id del editor");
				EditorsVistaController.mostrarEditores(editors);
				//se pide un id
				int temp = UtilConsole.pedirInt();
				//si el id existe cogemos el editor si no, no
				if (editors.containsKey(temp)) {
					editor = editors.get(temp);
				}
			}else{
				System.out.println("Opcion erronea");
			}
			
		}
		//creamos una categoria null
		Categoria categoria = null;
		//mientras sea null seguimos pidiendo una opcion
		while (categoria == null) {
			//mostramos las opciones
			System.out.println("1. Crear una nueva categoria");
			System.out.println("2. Usar una categoria existente");
			int opcion = UtilConsole.pedirInt();
			//dependiendo de la opcion elegida, llamamos a la funcion correspondiente
			if (opcion == 1) {
				//la funcion crea una categoria y nos la devuelve
				categoria = CategoriesVistaController.crearCategoria(categories);
			}else if (opcion == 2) {
				//muestra las categorias y nos devuelve la que escojamos
				categoria = CategoriesVistaController.getCategoria(categories);
			}else{
				System.out.println("Opcion erronea");
			}
		}
		//finalmente con los datos creamos el libro
		Llibre llibre = new Llibre(titol, descripcio, preuVenda, editor, categoria);
		//lo guardamos en el hashmap de libros
		llibres.put(titol, llibre);
		
	}

	/**
	 * devuelve la lista de libros
	 * @return hashmap de libros
	 */
	public static HashMap<String, Llibre> getLLibres(){
		return llibres;
	}
	
	/**
	 * Busca un libro y si lo encuentra lo devuelve
	 * @return
	 */
	public static Llibre getLLibre(String id){
		//verifica que el libro exista, caso contrario vuelve a preguntar hasta que se ponga un titulo correcto
		while (!llibres.containsKey(id)) {
			System.out.println("El libro "+id+" no existe! Prueba otro titulo");
			id = UtilConsole.pedirString();
		}
		//nos devuelve el libro
		return llibres.get(id);
	}
}
