package recursivitat;

/**
 * @author Álvaro Owen de la Quintana ProgramacionBasicaUF2 9 ene 2023
 */
public class Recursivas {

	public static void main(String[] args) {
		sumaRecursiu(7);
	}
	
	public static int sumaRecursiu(int numero) {
		if (numero > 1) {
			return numero * sumaRecursiu(numero - 1);
		} else {
			return 1;
		}
	}
}
