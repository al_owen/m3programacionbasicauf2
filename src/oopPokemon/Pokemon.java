package oopPokemon;

import java.util.ArrayList;
import java.util.Random;

public class Pokemon {

	public static int Creados;//es una variable statica, 
	//sirve para definir una variable a la que se puede 
	//acceder sin tener que crear un objeto
	
	//atributos del objeto
	/*Los atributos son variables que pertenecen al objeto,
	 * todos los objetos tendran los mismos atributos/variables, 
	 * pero cada uno tendrá un valor diferente.
	 * estas variables son globales, por lo tanto,
	 * podemos acceder desde cualquier parte de la clase 
	 * */
	private String nombre;
	private ArrayList<String> tipos;
	private Ataque[] ataques = new Ataque[4];
	private int nivel;
	private int hp, atck, def;
	private boolean debilitado;
	
	//constructores del objeto
	/*el constructor es una especie de funcion a la que se llama
	 * en el momento de crear un objeto, puede pedir parametros(valores)
	 * o puede no pedir nada.
	 * Al ser un tipo de funcion podemos tratarla como una funcion normal,
	 * por ejemplo, podemos llamar a otras funciones, podemos asignar valores,
	 * podemos hacer incluso calculos. Aunque, lo normal, es simplemente asignar valores. 
	 * */
	//constructor sin parametros
	public Pokemon() {
		//al no pedir parametros los asignamos aleatoriamente,
		//de lo contrario, serian null
		Creados++;
		Random r = new Random();
		nombre = "--";
		nivel = r.nextInt(1,101);
		hp = r.nextInt(100,301);
		atck = r.nextInt(50,151);
		def = r.nextInt(50,151);
		debilitado = false;
	}
	/*en este caso el constructor pide valores que asigna a los atributos del objeto
	 * */
	public Pokemon(String name, int level, int hp, int atk, int def) {
		Creados++;
		nombre = name;
		nivel = level;
		/*el 'this.' hace que se diferencie entre:
		*la variable local (del constructor) 'hp' y el atributo del objeto tambien llamado 'hp'.
		*si no se pone el 'this.' el programa no sabra que variable debe modificar
		*por lo tanto es posible que le asigne el valor de hp a si misma 
		*envés de modificar el atributo, por lo tanto, sería lo mismo que decir:
		*que una variable es igual a si misma.*/
		this.hp = hp;
		this.atck = atk;
		this.def = def;
		debilitado = false;
	}
	
	
	//funciones
	/*las funciones son las acciones que puede realizar cada objeto
	*son lineas de codigo que permiten tener acciones predeterminadas 
	*que establecen la logica del programa.*/
	public void atacar(Pokemon enemigo, int indiceAtk) {
		//calculamos el daño
		int damage = this.ataques[indiceAtk].getDamage()+atck;
		//enviamos el daño al enemigo
		enemigo.takeDamage(damage);
	}
	
	public void takeDamage(int damage) {
		//verificamos que el enemigo no esta debilitado
		if (!debilitado) {
			//reducimos el daño del hp
			this.hp -= (damage-def);
			System.out.println(nombre+ " ha recibido "+(damage-def)+" de daño");
			//si la vida es menor o igual a 0
			if (hp <= 0) {
				//le indicamos que la vida es 0 para no tener numeros negativos
				hp = 0;
				//indicamos que el pokemon ha sido debilitado
				debilitado = true;
			}
		}
	}
	
	//getters y setters
	/*Los getters y los setters son funciones habituales 
	 * que sirven para mantener la privacidad de los atributos.
	 * Mediante los getters, podemos obtener el valor de una variable
	 * sin tener que acceder directamente a la variable.
	 * Mediante los setters podemos modificar el valor de la varibale, 
	 * esta funcion nos sirve para controlar lo que se puede o no hacer en un atributo
	 * si por ejemplo, si queremos que una variable se pueda consultar para ver su valor pero 
	 * no queremos que cualquiera lo pueda modificar, creariamos un getter, pero no un setter.
	 *  */
	
	/*esta funcion recibe un valor y modifica el nombre 
	 * */
	protected void setNombre(String nuevoNombre) {
		this.nombre = nuevoNombre;
	}
	
	//esta funcion devuelve el nombre del pokemon 
	public String getNombre() {
		return this.nombre;
	}
	public ArrayList<String> getTipos() {
		return tipos;
	}
	public void setTipos(ArrayList<String> tipos) {
		this.tipos = tipos;
	}
	public Ataque[] getAtaques() {
		return ataques;
	}
	//en este caso es mejor poder pasarle simplemente un nuevo ataque y 
	//especificar la posicion enves de pasar una array entera. 
	//
	public void addAtaques(Ataque ataque, int posicion) {
		this.ataques[posicion] = ataque;
	}
	public int getNivel() {
		return nivel;
	}
	public void setNivel(int nivel) {
		this.nivel = nivel;
	}
	public int getHp() {
		return hp;
	}
	public void setHp(int hp) {
		this.hp = hp;
	}
	public int getAtck() {
		return atck;
	}
	public void setAtck(int atck) {
		this.atck = atck;
	}
	public int getDef() {
		return def;
	}
	public void setDef(int def) {
		this.def = def;
	}
	public boolean isDebilitado() {
		return debilitado;
	}
	public void setDebilitado(boolean debilitado) {
		this.debilitado = debilitado;
	}
	@Override
	public String toString() {
		return nombre +" tipos=" + tipos + ", ataques= " + ataques + ", nivel= " + nivel + ", hp= "
				+ hp + ", atck= " + atck + ", def= " + def + ", debilitado= " + debilitado;
	}
	
	
}
