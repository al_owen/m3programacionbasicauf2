package oopPokemon;

public class Ataque {

	//atributos de un ataque
	private String nombre;
	private int damage;
	private String tipo;
	
	public Ataque(String nombre, int damage, String tipo) {
		super();
		this.nombre = nombre;
		this.damage = damage;
		this.tipo = tipo;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int getDamage() {
		return damage;
	}
	public void setDamage(int damage) {
		this.damage = damage;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	@Override
	public String toString() {
		return "Ataque= " + nombre + ", daño=" + damage + ", tipo=" + tipo+" ";
	}
	
	
}
