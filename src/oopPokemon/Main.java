package oopPokemon;

public class Main {

	public static void main(String[] args) {
		/*Creamos un pokemon pasandole valores
		 * estamos llamando al constructor que pide estos valores
		 * */
		Pokemon pokachu = new Pokemon("Pokachu",99, 200, 80, 95);
		//para que pokachu tenga un ataque
		//primero creamos el ataque
		Ataque impactrueno = new Ataque("Impactrueno", 50, "electrico");
		//y despues se lo pasamos
		pokachu.addAtaques(impactrueno, 0);
		
		/*creamos un pokemon pero no le pasamos valores
		 * estamos llamando al constructor que no pide valores
		 * */
		Pokemon dikorita = new Pokemon();
		dikorita.setNombre("Dikorita");
		
		//Si creamos un pokemon, pero no lo guardamos en una variable
		//no tendremos una refencia a este pokemon y por lo tanto, no podremos llamarlo 
		new Pokemon();
		//mostramos la variable estatica
		//Para llamar a una variable static, no hace falta crear un objeto
		//Se llaman directamente llamando a la Clase.Variable
		System.out.println(Pokemon.Creados);
		//llamamos a la funcion getNombre() de pokachu
		//solo nos mostrará el nombre de pokachu
		System.out.println(pokachu.getNombre());
		//llamamos a la funcion getNombre() de dikorita
		//solo nos mostrará el nombre de dikorita
		System.out.println(dikorita.getNombre());
		
		//para que haya un combate haremos que pokachu ataque a dickorita
		//primero mostramos los stats
		System.out.println(pokachu);
		System.out.println(dikorita);
		//hacemos que pokachu ataque
		pokachu.atacar(dikorita, 0);
		//volvemos a mostrar los stats
		System.out.println(pokachu);
		System.out.println(dikorita);
		
		
		
		
	}

}
